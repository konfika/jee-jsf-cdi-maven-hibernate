package rhodri.managers;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import rhodri.model.User;

@Stateless
public class UserManager {
	
	@PersistenceContext
	EntityManager entityManager;

	public User getUser(String username) {
		Query query = entityManager.createQuery("from User where username = :USERNAME");
		query.setParameter("USERNAME", username);
		
		// Could use .getSingleResult() but then we'd have to handle NoResultException
		List<User> userList = (List<User>)query.getResultList();
		if (userList.size() > 0) {
			return userList.get(0);
		}
		return null;
	}
}
