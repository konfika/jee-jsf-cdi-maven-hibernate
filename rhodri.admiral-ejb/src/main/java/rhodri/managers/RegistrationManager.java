package rhodri.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import rhodri.exceptions.ValidationException;
import rhodri.model.User;

@Stateless
public class RegistrationManager {

	@Inject
	UserManager userManager;
	
	@PersistenceContext
	EntityManager entityManager;

	public User register(String username, String password) throws ValidationException {
		validateRegistration(username, password);

		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		entityManager.persist(user);
		return user;
	}

	public void validateRegistration(String username, String password) throws ValidationException {

		List<String> validationErrorList = new ArrayList<String>();
		if (username != null) {
			User existingUser = userManager.getUser(username);
			if (existingUser != null) {
				validationErrorList.add("User already exists");
			}
		} else {
			validationErrorList.add("Please supply a username");
		}

		if (password != null) {
			if (password.length() < 6) {
				validationErrorList.add("Password too short, must be at least 6 characters long");
			}
		} else {
			validationErrorList.add("Please supply a password");
		}
		
		if (validationErrorList.size() > 0) {
			throw new ValidationException(validationErrorList);
		}
	}
}
