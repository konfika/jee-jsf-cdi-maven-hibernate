package rhodri.exceptions;

import java.util.List;
import java.util.logging.Logger;

public class ValidationException extends Exception{
	
	Logger log = Logger.getGlobal();
	/**
	 * 
	 */
	private static final long serialVersionUID = -7126232510203130846L;
	private List<String> causes;
	
	public ValidationException(List<String> causes) {
		this.causes = causes;
	}
	
	public void printValidationErrors() {
		for(String c : causes) {
			log.severe(c);
		}
	}
	
	public List<String> getCauses() {
		return causes;
	}
}
