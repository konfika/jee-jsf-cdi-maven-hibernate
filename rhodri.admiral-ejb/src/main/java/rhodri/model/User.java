package rhodri.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="users",
		uniqueConstraints= {@UniqueConstraint(columnNames= {"userpk"})})
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="userpk", nullable=false, unique=true, length=11)
	private int userpk;
	
	@Column(name="username", length=128, nullable=false)
	private String username;
	
	@Column(name="password", length=128, nullable=false)
	private String password;
	
	@Column(name="versionstart")
	private Date versionstart;
	
	@Column(name="versionend")
	private Date versionend;
	
	@Column(name="userid")
	private int userid;
	
	public int getUserpk() {
		return userpk;
	}
	public void setUserpk(int userpk) {
		this.userpk = userpk;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getVersionstart() {
		return versionstart;
	}
	public void setVersionstart(Date versionstart) {
		this.versionstart = versionstart;
	}
	public Date getVersionend() {
		return versionend;
	}
	public void setVersionend(Date versionend) {
		this.versionend = versionend;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	
}
