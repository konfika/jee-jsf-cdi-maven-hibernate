package rhodri.user.registration;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import rhodri.exceptions.ValidationException;
import rhodri.managers.RegistrationManager;

@Named(value="registrationBean")
@SessionScoped
public class RegistrationBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8317282439056349250L;

	@Inject
	RegistrationManager registrationManager;
	
	private String username;
	private String password;
	
	 
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void register() {
		try {
			//process and store to DB
			registrationManager.register(username, password);
			
		} catch (ValidationException e) {
			e.printValidationErrors();
		}
	}
}
